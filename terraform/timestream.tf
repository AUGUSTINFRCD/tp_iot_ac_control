resource "aws_timestreamwrite_database" "iot" {
  database_name = "iot"
}


# create aws_timestreamwrite_table "iot"
resource aws_timestreamwrite_table temperaturesensor {
    database_name = "iot"
    table_name    = "temperaturesensor"
    retention_properties {
        magnetic_store_retention_period_in_days = 730
        memory_store_retention_period_in_hours  = 24
    }
    tags = {
        Name = "temperaturesensor"
    }
}


