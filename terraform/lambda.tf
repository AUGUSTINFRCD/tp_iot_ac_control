# aws_lambda_function to control air conditioner
resource "aws_lambda_function" "lambda" {
  filename      = "files/empty_package.zip"
  function_name = "ac_control_lambda"
  handler       = "ac_control_lambda.lambda_handler"
  runtime = "python3.7"
  role = aws_iam_role.lambda_role.arn
}

# aws_cloudwatch_event_rule scheduled action every minute
resource "aws_cloudwatch_event_rule" "everyMinute" {
    schedule_expression = "rate(1 minute)"
}

# aws_cloudwatch_event_target to link the schedule event and the lambda function
resource "aws_cloudwatch_event_target" "eventTarget" {
  target_id = "lambda"
  rule      = aws_cloudwatch_event_rule.everyMinute.name
  arn       = aws_lambda_function.lambda.arn
}

# aws_lambda_permission to allow CloudWatch (event) to call the lambda function
resource "aws_lambda_permission" "allowCloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.everyMinute.arn
}
