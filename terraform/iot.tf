resource "aws_iot_certificate" "cert" {
  active = true
}

resource "aws_iot_policy" "pub-sub" {
  name = "PubSubToAnyTopic"
  policy = file("${path.module}/files/iot_policy.json")
}

resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pub-sub.name
  target = aws_iot_certificate.cert.arn
}

resource "aws_iot_thing" "house" {
  name = "house"
}

resource "aws_iot_thing_principal_attachment" "thing_attachment" {
  principal = aws_iot_certificate.cert.arn
  thing = aws_iot_thing.house.name
}

data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}


# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "temperature_rule" {
  name = "TemperatureRule"
  description = "Rule to insert messages into DynamoDB"
  enabled = true
  sql = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.temperature-table.name
    }
  }
}


# aws_iot_topic_rule rule for sending valid data to Timestream

resource "aws_iot_topic_rule" "temperature_timestream_rule" {
  name = "TemperatureRuleTimeStream"
  description = "Rule to insert messages into Timestream"
  enabled = true
  sql = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

  timestream {
    database_name = "iot"
    role_arn = aws_iam_role.iot_role.arn
    table_name = aws_dynamodb_table.temperature-table.name

    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
  }
}


###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
